# Notes on Images

"product-icon-url" image is to be preferably 64x64 (or at least height 64px) and optimised with Image Optim or similar tool. Consideration shouild be paid in future to use lazy loading and something like Lozad for asset heavy pages where not all imagery is above the fold.

## Tools/Commands

Currently using ImageMagick to resize files on CLI: convert -resize x64 abuseipdb.png abuseipdb.png

and also get file information with standard: file abuseipdb.png 

if the only thing available is an SVG then using: convert -density 1200 -resize x64 cherwell.svg cherwell.png

Batch resizing: mogrify -format png -path ./transformed -resize x64 *.png

*Note:* Will also use ImageOptim on OSX to batch optimise them once downloaded.

## SVGs are broken in WebFlow API

SVGs unfortunately, even when uploaded via the UI, can not be specifically URL referenced in the API, as the resulting file content type is interpreted as JSON by WebFlow and served as such by their web servers.

## PNGs work but need to be uploaded to WebFlow

Currently to upload an image asset to WebFlow we need to use the UI and then get the WebFlow asset URL and specify it in our JSON file definitions for each image type.

In the future we will work out a way to upload an image before the automation attempts to create the full integration item in WebFlow whereby this method would let us specify an external source and then just the fileID in the main image fields.

Note: We can not rely on external image hosting nor can we ask WebFlow to re-upload an external image each time (as the API is PUTing each time). 

## WEBP

*Note:* Can I used? [https://caniuse.com/#feat=webp](https://caniuse.com/#feat=webp)
*Note_II:* Google recommendation [https://developers.google.com/speed/webp/faq](https://developers.google.com/speed/webp/faq)
